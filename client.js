const mq = require('./conn.js');
var args = process.argv.slice(2);
console.log(args);
if(args.length == 0){
    console.log("发送参数为空……");
    process.exit(1);
}
const channelName = 'rpc_queue2';


async function demo()
{
    try {
        let conn = await mq.conn();
        conn.createChannel(function(err,ch) {
            ch.assertQueue('',{ exclusive:true },function(err,q) {
                //随机数唯一值
                // console.log(q);
                var corr = generateUuid();
                var num = args[0];
                console.log('[x] Requesting fib(%d)',num);
                //replyTo:q.queue 设置
                ch.sendToQueue(channelName,Buffer.from(num.toString()),{correlationId:corr,replyTo:q.queue});
                //接受服务端消息
                ch.consume(q.queue,function(msg) { 
                    if(msg.properties.correlationId == corr){
                        console.log('服务端返回：',msg.content.toString());
                        setTimeout(function() {conn.close(); process.exit(0)},500);
                    }
                },{ noAck:true });
            });
        });
    } catch (error) {
        console.log('mq异常：'+error);
    }
}
function  generateUuid() {
    return  Math.random().toString()+
        Math.random().toString()+
        Math.random().toString();
}

demo();