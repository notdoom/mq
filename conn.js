const config = require("./config.js");
const amqplib = require('amqplib/callback_api');
const {promisify}   = require("util"); 
const amqplibPromise = promisify(amqplib.connect);
const conn = async function()
{
    // return await new Promise( (resolve) => {
    //     amqplib.connect(config,function(err,conn){
    //         if(err)
    //             throw new Error(err);
    //         resolve(conn);
    //     });
    // });
    return await amqplibPromise(config);
}

module.exports = {
    conn
}