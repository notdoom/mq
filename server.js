const mq = require('./conn.js');
const {promisify}   = require("util"); 
const channelName = 'rpc_queue';

async function demo()
{
    try {
        let conn = await mq.conn();
        conn.createChannel(function(err,ch){ //创建通道
            ch.assertQueue(channelName,{ durable:false }); //连接队列
            ch.prefetch(1); //每次只取一条
            console .log('[x]等待RPC请求');
            ch.consume(channelName,function  reply(msg) { //队列消费
                var n = msg.content.toString();
                // console.log("msg：",msg);
                console.log(`队列：${channelName}`)
                console.log("接收参数：",n);
                console.log("ID：",msg.properties.correlationId);
                ch.sendToQueue(msg.properties.replyTo,Buffer.from('傻逼'),{correlationId:msg.properties.correlationId}); //发送队列
                ch.ack(msg);
            });
        });
    } catch (error) {
        console.log('mq异常：'+error);
    }
}

demo();